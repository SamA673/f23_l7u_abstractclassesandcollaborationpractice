package com.example;

import org.junit.Test;
import static org.junit.Assert.*;

public class VegetarianSandwichTest {

  /* 
     * Test the constructor method of the VegetarianSandwich abstract class using a TofuSandwich.
     * This method verifies that the constructor correctly initializes a TofuSandwich with Tofu as a filling. 
     */
    @Test 
    public void testConstructor(){
        TofuSandwich sandwich = new TofuSandwich();
        assertEquals("Tofu", sandwich.getFilling()); 
    }


    /* 
     * Test the addFilling method of the VegetarianSandwich class.
     * This method verifies that addFilling successfully adds a filling if a valid (non-meat) filling is provided.  
     */
    @Test
    public void testAddFilling_ValidTopping() {
        TofuSandwich sandwich = new TofuSandwich();
        sandwich.addFilling("cucumber");
        assertEquals("Tofu cucumber", sandwich.getFilling());
    }

    /* 
     * Test the addFilling method of the VegetarianSandwich class.
     * This method verifies that addFilling successfully throws an exception if an invalid (meat) filling is added. 
     */
    @Test
    public void testAddFilling_InvalidTopping() {
        try{
            CaesarSandwich sandwich = new CaesarSandwich();
            sandwich.addFilling("chicken"); 
            fail("addFilling was a success but shouldn't have been because meat was added. ");
            } catch(IllegalArgumentException e){
                // addFilling() failed successfully!
            }
    }


    /* 
     * Test the isVegetarian method of the VegetarianSandwich class.
     * This method verifies that isVegetarian returns true for an empty (meaning no fillings) VegetarianSandwich objects.
     */
    @Test
    public void testIsVegetarian() {
        CaesarSandwich sandwich = new CaesarSandwich();
        assertTrue(sandwich.isVegetarian()); // Post override being commented out...! 
    }


    /*  
     * Test the isVegan method of the VegetarianSandwich class. 
     * This method verifies that isVegan returns true when the sandwich has no eggs or cheese. 
     */
    @Test
    public void testIsVegan_ValidFilling() {
        CaesarSandwich sandwich = new CaesarSandwich();
        sandwich.addFilling("lettuce"); 
        assertTrue(sandwich.isVegan()); 
    }

    /*
     * Test the isVegan method of the VegetarianSandwich class.
     * This method verifies that isVegan returns false when the sandwich has cheese in it. 
     */
    @Test
    public void testIsVegan_withCheese() {
        CaesarSandwich  sandwich = new CaesarSandwich ();
        sandwich.addFilling("cheese");
        assertFalse(sandwich.isVegan()); 
    }

    /*
     * Test the isVegan method of the VegetarianSandwich class. 
     * This method verifies that isVegan returns false when the sandwich has lettuce and egg in it. 
     */
    @Test
    public void testIsVegan_withEgg() {
        CaesarSandwich sandwich = new CaesarSandwich();
        sandwich.addFilling("lettuce"); 
        sandwich.addFilling("egg");
        assertFalse(sandwich.isVegan()); 
    }



    /*
     * Test the get filling method of the VegetarianSandwich class.
     * This method ensures that getFilling returns the correct string when there is:
     *          - No Filling ("")
     *          - One filling (Lettuce)
     *          - Two Fillings (Lettuce and egg)
     */
    @Test
    public void testGetFilling() {
        TofuSandwich sandwich = new TofuSandwich();
        assertEquals("Tofu", sandwich.getFilling()); 

        sandwich.addFilling("lettuce");
        assertEquals("Tofu lettuce", sandwich.getFilling()); 

        sandwich.addFilling("egg");
        assertEquals("Tofu lettuce egg", sandwich.getFilling()); 
    }
}