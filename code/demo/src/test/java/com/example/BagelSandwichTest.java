package com.example;

import static org.junit.Assert.*;
import org.junit.Test;

public class BagelSandwichTest {
    @Test
    public void bagelSandwichConstructorTest() {
        BagelSandwich bagelSandwich = new BagelSandwich();

        assertEquals("", bagelSandwich.getFilling());
    }

    @Test
    public void addFillingTestReturnsString() {
        BagelSandwich bagelSandwich = new BagelSandwich();
        bagelSandwich.addFilling("Cheese");

        assertEquals("Cheese", bagelSandwich.getFilling());
    }

    @Test
    public void addFillingTestReturnsConcatString() {
        BagelSandwich bagelSandwich = new BagelSandwich();
        bagelSandwich.addFilling("Cheese");
        bagelSandwich.addFilling("Tomato");

        assertEquals("Cheese Tomato", bagelSandwich.getFilling());
    }

    @Test
    public void isVegetarianTestReturnsException() {
        BagelSandwich bagelSandwich = new BagelSandwich();
        try {
            bagelSandwich.isVegetarian();
            fail("UnsupportedOperationException should have been thrown and caught.");
        } catch (UnsupportedOperationException e) {
            // Caught error successfully
        }
    }
}
