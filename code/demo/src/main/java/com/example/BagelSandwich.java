package com.example;


public class BagelSandwich implements ISandwich {
        private String filling; 

    // Constructor
    public BagelSandwich(){
        this.filling = ""; 
    }

    // Interface methods....
    public String getFilling(){
        return this.filling;

    }
    public void addFilling(String topping){
        if(this.filling.equals("")){
        this.filling += topping; 
        } else {
            this.filling += " " + topping; 
        }
    }

    public boolean isVegetarian(){
        throw new UnsupportedOperationException("isVegetarian is not supported by BagelSandwich");
    }

    // Others...



}
