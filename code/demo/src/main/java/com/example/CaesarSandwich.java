package com.example;

public class CaesarSandwich extends VegetarianSandwich {
    public CaesarSandwich() {
        addFilling("Caesar dressing");
    }

    @Override
    public String getProtein() {
        return "Anchovies";
    }

    //@Override
    //public boolean isVegetarian() {
    //    return false;
    //}
}