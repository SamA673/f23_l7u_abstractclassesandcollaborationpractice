package com.example;

public abstract class VegetarianSandwich implements ISandwich {
    private String filling;

    public VegetarianSandwich() {
        this.filling = "";
    }
    
    public void addFilling(String topping) {
        final String[] meat = {"chicken", "beef", "fish", "meat", "pork"};
        for (String meatType : meat) {
            if(meatType.equals(topping)) {
                throw new IllegalArgumentException("There is no meat in a vegetarian sandwich!");
            }
        }
        if(this.filling.equals("")){
            this.filling += topping; 
        }
        else {
            this.filling += " " + topping; 
        }
    }

    public String getFilling() {
        return this.filling;
    }

    public final boolean isVegetarian() {
        return true;
    }

    public boolean isVegan() {
       String[] fillings = this.filling.split(" ");
       for (String filling : fillings) {
            if (filling.equals("egg") || filling.equals("cheese")) {
                return false;
            }
       }
       return true;
    }

    public abstract String getProtein();
}
