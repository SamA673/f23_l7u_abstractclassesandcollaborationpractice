package com.example;


/** 
 *  App is a class  used to test different Sandwiches!
 *  @author Samin Ahmed
 *  @version 10/20/2023
 */

public class App {
    public static void main(String[]args){

        // VegetarianSandwich sandwhich = new VegetarianSandwich(); WILL NOT RUN

        ISandwich sandy = new TofuSandwich();
            

        if (sandy instanceof ISandwich) {
            System.out.println("The TofuSandwich is an instance of the ISandwich interface.");
        } else {
            System.out.println("The TofuSandwich is not an instance of the ISandwich interface.");
        }


        if (sandy instanceof VegetarianSandwich) {
            System.out.println("The TofuSandwich is an instance of the VegetarianSandwich interface.");
        } else {
            System.out.println("The TofuSandwich is not an instance of the VegetarianSandwich interface.");
        }



        TofuSandwich tofu = new TofuSandwich();
        // tofu.addFilling("chicken");          Gives runtime error because there can't be meat in a vegetarian sandwich.
    
        tofu.isVegan(); 

        VegetarianSandwich vegetarianSandwich = (VegetarianSandwich) sandy; 
        vegetarianSandwich.isVegan();


        VegetarianSandwich newVeg = new CaesarSandwich();
        System.out.println( newVeg.isVegetarian() );
    }
}
