package com.example;

public class TofuSandwich extends VegetarianSandwich{
    public TofuSandwich(){
        this.addFilling("Tofu");
    }

    public String getProtein(){
        return "Tofu"; 
    }

    //@Override
    //public boolean isVegetarian(){
    //    return false; 
    //}

}