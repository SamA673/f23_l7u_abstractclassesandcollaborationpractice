package com.example;


/** 
 *  ISandwich is an interface used to hold methods for
 *  sandwiche types!
 *  @author Samin Ahmed
 *  @version 10/17/2023
 */

public interface ISandwich {
    
    String getFilling();
    void addFilling(String topping); 
    boolean isVegetarian(); 

}

   
